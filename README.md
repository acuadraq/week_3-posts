# Week_3 Posts

![](src/img/Screenshot.png)

## Description

This is the weekly assignment for the third week. Its a list of posts. This are the main points of the page:

- All of the posts are retrieve from an json api server.
- On the main page we have a section with the last 3 posts, another section with all of the posts with a search and tag filter and ordered DESC. And also a button to go to create a post
- We have a create page with all the fields that are required to the post
- We have a edit page to edit any post you want
- You can comment on any post
- You can like any post
- You can delete any post

If you want to tested in real time use this url: https://week-3-posts.vercel.app/
