class Coffee {
  constructor(name) {
    this.name = name;
  }
}

class CoffeeBuilder {
  constructor(name) {
    this.coffee = new Coffee(name);
  }

  setEspresso() {
    this.coffee.espresso = true;
    return this;
  }

  setMilkFoam() {
    this.coffee.milkFoam = true;
    return this;
  }

  setWhippedCream() {
    this.coffee.whippedCream = true;
    return this;
  }

  setSteamedMilk() {
    this.coffee.steamedMilk = true;
    return this;
  }

  setSteamedHalfHalf() {
    this.coffee.steamedHalfHalf = true;
    return this;
  }

  setChocolateSyrup() {
    this.coffee.chocolateSyrup = true;
    return this;
  }

  setHotWater() {
    this.coffee.hotWater = true;
  }

  setCoffe() {
    this.coffee.cafe = true;
    return this;
  }

  setMilk() {
    this.coffee.milk = true;
    return this;
  }

  build() {
    return this.coffee;
  }
}

const mocha = new CoffeeBuilder("Mocha")
  .setEspresso()
  .setChocolateSyrup()
  .setSteamedMilk()
  .setWhippedCream()
  .build();
console.log(mocha);

const expresso = new CoffeeBuilder("Expresso").setEspresso().build();
console.log(expresso);

const macchiato = new CoffeeBuilder("Macchiato").setEspresso().build();
console.log(macchiato);

const latte = new CoffeeBuilder("Latte")
  .setEspresso()
  .setSteamedMilk()
  .setMilkFoam()
  .build();
console.log(latte);
