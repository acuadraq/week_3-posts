class FetchApi {
  constructor() {}

  getPosts(searchFilter) {
    if (searchFilter) {
      return fetch(
        `https://json-project-posts.herokuapp.com/posts?_sort=id&_order=desc&q=${searchFilter}`
      )
        .then(res => res.json())
        .catch(err => console.log(err));
    } else {
      return fetch(
        "https://json-project-posts.herokuapp.com/posts?_sort=id&_order=desc"
      )
        .then(res => res.json())
        .catch(err => console.log(err));
    }
  }
  getComments(id) {
    return fetch(
      `https://json-project-posts.herokuapp.com/posts/${id}/comments`
    )
      .then(res => res.json())
      .catch(err => console.log(err));
  }
  getTags(searchFilter) {
    if (searchFilter) {
      return fetch(
        `https://json-project-posts.herokuapp.com/tags?q=${searchFilter}`
      )
        .then(res => res.json())
        .catch(err => console.log(err));
    } else {
      return fetch("https://json-project-posts.herokuapp.com/tags")
        .then(res => res.json())
        .catch(err => console.log(err));
    }
  }

  getDetailPost(id) {
    return fetch(`https://json-project-posts.herokuapp.com/posts/${id}`)
      .then(res => res.json())
      .catch(err => console.log(err));
  }

  getUsers() {
    return fetch(`https://json-project-posts.herokuapp.com/users`)
      .then(res => res.json())
      .catch(err => console.log(err));
  }

  getAuthors() {
    return fetch(`https://json-project-posts.herokuapp.com/authors`)
      .then(res => res.json())
      .catch(err => console.log(err));
  }

  postComment(commentObject) {
    fetch("https://json-project-posts.herokuapp.com/comments", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Accept: "application/json",
      },
      body: JSON.stringify(commentObject),
    })
      .then(resp => location.reload())
      .catch(err => console.log(err));
  }

  deletePost(id) {
    fetch(`https://json-project-posts.herokuapp.com/posts/${id}`, {
      method: "DELETE",
    })
      .then(resp => window.location.replace("/index.html"))
      .catch(err => console.log(err));
  }

  patchLikes(likeObject, id) {
    fetch(`https://json-project-posts.herokuapp.com/posts/${id}`, {
      method: "PATCH",
      headers: {
        "Content-Type": "application/json",
        Accept: "application/json",
      },
      body: JSON.stringify(likeObject),
    })
      .then(resp => location.reload())
      .catch(err => console.log(err));
  }

  putPost(postObject, id) {
    fetch(`https://json-project-posts.herokuapp.com/posts/${id}`, {
      method: "PUT",
      headers: {
        "Content-Type": "application/json",
        Accept: "application/json",
      },
      body: JSON.stringify(postObject),
    })
      .then(resp => window.location.replace(`/posts.html?id=${id}`))
      .catch(err => console.log(err));
  }

  createPost(postObject) {
    fetch(`https://json-project-posts.herokuapp.com/posts`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Accept: "application/json",
      },
      body: JSON.stringify(postObject),
    })
      .then(resp => window.location.replace("/index.html"))
      .catch(err => console.log(err));
  }
}

const posts = new FetchApi();
Object.freeze(posts);
export { posts };
