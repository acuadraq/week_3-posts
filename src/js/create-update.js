import { debounce } from "./utils/debounce.js";
import {
  renderAuthors,
  renderDetailPost,
  renderTags,
} from "./makingRequests.js";
import { searchTags, showTags } from "./tags_handle.js";
import { tagSearch, loadingAnimation } from "./utils/variables.js";
import { handleForm } from "./form_handle.js";

const form = document.querySelector(".form");
const id = new URLSearchParams(window.location.search).get("id");
const cardForm = document.querySelector(".card-container");
const authorSelect = document.getElementById("author");
let titleOfPage = document.querySelector(".titleOfThePage");
const authorsGet = renderAuthors();
const tagsSaved = [];
const tagsGet = renderTags();
let likes = 0;

const formUpdate = async () => {
  const post = await renderDetailPost(id);
  form.title.value = post.title;
  form.subTitle.value = post.subTitle;
  form.image.value = post.image;
  form.body.value = post.body;
  const date = post.createDate.split("/");
  form.createDate.value = date.join("-");
  const authors = await authorsGet;
  for (let author of authors) {
    if (author.id == post.author) {
      authorSelect.innerHTML += `<option value=${author.id} selected>${author.name} ${author.lastName}</option>`;
    } else {
      authorSelect.innerHTML += `<option value=${author.id}>${author.name} ${author.lastName}</option>`;
    }
  }
  const tags = await tagsGet;
  for (let postTags of post.tags) {
    for (let tag of tags) {
      if (tag.id == postTags) {
        tagsSaved.push([tag.id, tag.name]);
        showTags(tag.id, tag.name);
      }
    }
  }
  likes = post.likes;
  loadingAnimation.style.display = "none";
  cardForm.style.display = "flex";
};

const formCreate = async () => {
  const authors = await authorsGet;
  for (let author of authors) {
    authorSelect.innerHTML += `<option value=${author.id}>${author.name} ${author.lastName}</option>`;
  }
  loadingAnimation.style.display = "none";
  cardForm.style.display = "flex";
};

if (id) {
  loadingAnimation.style.display = "block";
  titleOfPage.textContent = "Update Post";
  formUpdate();
} else {
  loadingAnimation.style.display = "block";
  titleOfPage.textContent = "Create Post";
  formCreate();
}

tagSearch.addEventListener(
  "input",
  debounce(() => {
    searchTags(tagSearch.value);
  }, 500)
);

form.addEventListener("submit", e => {
  e.preventDefault();
  if (id) {
    handleForm(form, tagsSaved, likes, id);
  } else {
    handleForm(form, tagsSaved, likes);
  }
});

export { tagsSaved, form };
