import {
  renderDetailPost,
  renderComments,
  renderUsers,
} from "./makingRequests.js";
import {
  loadingAnimation,
  popUpDelete,
  closeButton,
  cancelButton,
} from "./utils/variables.js";
import { posts } from "./classes.js";
import { displayPost, displayComment } from "./showDataDetails.js";

const id = new URLSearchParams(window.location.search).get("id");
const selectUser = document.querySelector(".filterUser");
const form = document.querySelector(".comment-form");
const showModalButton = document.querySelector(".modal__button");
const confirmDelete = document.querySelector(".confirm--delete");
const editButton = document.querySelector(".button--edit");
const users = renderUsers();

const getDataOfPost = async () => {
  loadingAnimation.style.display = "block";
  const post = await renderDetailPost(id);
  displayPost(post);
};

const getComments = async () => {
  const comments = await renderComments(id);
  displayComment(comments);
};

const getUsers = async () => {
  const listUsers = await users;
  for (let user of listUsers) {
    selectUser.innerHTML += `<option value=${user.id}>${user.name}</option>`;
  }
};

const createComment = async e => {
  e.preventDefault();
  const commentPost = {
    comment: form.content.value,
    postId: id,
    user: form.user.value,
  };
  await posts.postComment(commentPost);
};

const deletePost = async () => {
  await posts.deletePost(id);
};

const addLike = async () => {
  const post = await renderDetailPost(id);
  const objectLike = {
    likes: post.likes + 1,
  };
  await posts.patchLikes(objectLike, id);
};

const showModal = e => {
  e.stopPropagation();
  popUpDelete.style.display = "flex";
};

form.addEventListener("submit", createComment);
showModalButton.addEventListener("click", showModal);

closeButton.addEventListener(
  "click",
  () => (popUpDelete.style.display = "none")
);
cancelButton.addEventListener(
  "click",
  () => (popUpDelete.style.display = "none")
);

confirmDelete.addEventListener("click", deletePost);
export const addEvent = () => {
  document.querySelector(".heart--button").addEventListener("click", addLike);
};

getDataOfPost();
getComments();
getUsers();
