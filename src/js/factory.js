class CommentFactory {
  constructor(name, user, comment) {
    if (name === "Anakin") {
      return new AdminFactor(user, comment);
    } else if (name === "Padme") {
      return new Reporter(user, comment);
    } else if (name === "Obi-Wan") {
      return new Guest(user, comment);
    } else if (name === "Gon") {
      return new Developer(user, comment);
    } else if (name === "Eren") {
      return new Maintainer(user, comment);
    }
  }
}

class AdminFactor {
  constructor(user, comment) {
    this.html = `
    <div>
    <p>
        <span>
            <img
            src="./src/img/icons/anakinicon.png"
            width="36"
            height="36"
            alt=""
            />
        </span>
        <span class="comments-user">${user.name} ${user.lastName} | ADMIN</span>
        </p>
        <hr />
        <p class="comments-content">
            ${comment.comment}
        </p>
    </div>
    `;
  }
}

class Reporter {
  constructor(user, comment) {
    this.html = `
    <div>
    <p>
        <span>
            <img
            src="./src/img/icons/padmeicon.png"
            width="36"
            height="36"
            alt=""
            />
        </span>
        <span class="comments-user">${user.name} ${user.lastName} | REPORTER</span>
        </p>
        <hr />
        <p class="comments-content">
            ${comment.comment}
        </p>
    </div>
    `;
  }
}

class Guest {
  constructor(user, comment) {
    this.html = `
    <div>
    <p>
        <span>
            <img
            src="./src/img/icons/obiwanicon.png"
            width="36"
            height="36"
            alt=""
            />
        </span>
        <span class="comments-user">${user.name} ${user.lastName} | GUEST</span>
        </p>
        <hr />
        <p class="comments-content">
            ${comment.comment}
        </p>
    </div>
    `;
  }
}

class Developer {
  constructor(user, comment) {
    this.html = `
    <div>
    <p>
        <span>
            <img
            src="./src/img/icons/gonicon.png"
            width="36"
            height="36"
            alt=""
            />
        </span>
        <span class="comments-user">${user.name} ${user.lastName} | DEVELOPER</span>
        </p>
        <hr />
        <p class="comments-content">
            ${comment.comment}
        </p>
    </div>
    `;
  }
}

class Maintainer {
  constructor(user, comment) {
    this.html = `
      <div>
      <p>
          <span>
              <img
              src="./src/img/icons/erenicon.png"
              width="36"
              height="36"
              alt=""
              />
          </span>
          <span class="comments-user">${user.name} ${user.lastName} | MAINTAINER</span>
          </p>
          <hr />
          <p class="comments-content">
              ${comment.comment}
          </p>
      </div>
      `;
  }
}

export { CommentFactory };
