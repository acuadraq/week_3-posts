import { posts } from "./classes.js";

const getTags = (tagsSaved, form, post) => {
  if (tagsSaved.length == 0) {
    form.tags.nextElementSibling.style.display = "block";
  } else {
    form.tags.nextElementSibling.style.display = "none";
    const tags = tagsSaved.map(item => item[0]);
    post.tags = tags;
  }
};

const validate = (fieldValue, objectProperty, post) => {
  if (fieldValue.value.trim() == "") {
    fieldValue.nextElementSibling.style.display = "block";
    return;
  } else {
    fieldValue.nextElementSibling.style.display = "none";
    switch (objectProperty) {
      case "title":
        post.title = fieldValue.value;
        break;
      case "subTitle":
        post.subTitle = fieldValue.value;
        break;
      case "body":
        post.body = fieldValue.value;
        break;
    }
  }
};

const handleForm = async (form, tagsSaved, likes, id) => {
  const post = {};
  validate(form.title, "title", post);
  validate(form.subTitle, "subTitle", post);
  post.image = form.image.value;
  validate(form.body, "body", post);
  post.createDate = form.createDate.value;
  post.likes = likes;
  post.author = form.author.value;
  getTags(tagsSaved, form, post);
  if (Object.keys(post).length == 8) {
    if (id) {
      await posts.putPost(post, id);
    } else {
      await posts.createPost(post);
    }
  }
};

export { handleForm };
