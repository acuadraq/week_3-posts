import { posts } from "./classes.js";

const renderPosts = async searchFilter => {
  if (searchFilter) {
    return await posts.getPosts(searchFilter);
  } else {
    return await posts.getPosts();
  }
};
const renderTags = async searchFilter => {
  if (searchFilter) {
    return await posts.getTags(searchFilter);
  } else {
    return await posts.getTags();
  }
};

const renderDetailPost = async id => await posts.getDetailPost(id);

const renderComments = async id => await posts.getComments(id);

const renderUsers = async () => await posts.getUsers();

const renderAuthors = async () => await posts.getAuthors();

export {
  renderPosts,
  renderTags,
  renderDetailPost,
  renderComments,
  renderUsers,
  renderAuthors,
};
