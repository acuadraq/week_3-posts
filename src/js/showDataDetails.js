import { renderTags, renderUsers, renderAuthors } from "./makingRequests.js";
import {
  templatePost,
  postContainer,
  loadingAnimation,
  commentSection,
  noCommentsWarning,
} from "./utils/variables.js";
import { addEvent } from "./details.js";
import { CommentFactory } from "./factory.js";
const buttonsContainer = document.querySelector(".div__buttons");
const buttonEdit = document.querySelector(".button--edit");
const tags = renderTags();
const users = renderUsers();
const authors = renderAuthors();
let commentFactor;

const displayPost = post => {
  let clone = templatePost.content.cloneNode(true);
  let img = clone.querySelector(".card__image");
  img.setAttribute("src", `${post.image}`);
  let divTags = clone.querySelector(".card-tags");
  tags.then(resp => {
    for (let tagID of post.tags) {
      const spanTag = document.createElement("span");
      spanTag.classList.add("tag");
      for (let tag of resp) {
        if (tag.id == tagID) spanTag.textContent = tag.name;
      }
      divTags.appendChild(spanTag);
    }
  });
  let tittle = clone.querySelector(".card-title");
  tittle.textContent = post.title;
  let subtitle = clone.querySelector(".card__subtitle");
  subtitle.textContent = `${post.subTitle}`;
  let likes = clone.querySelector(".likes");
  likes.textContent = `${post.likes} likes`;
  let description = clone.querySelector(".card__description");
  description.textContent = `${post.body}`;
  let authorText = clone.querySelector(".post__author");
  authors.then(resp => {
    for (let author of resp) {
      if (author.id == post.author)
        authorText.textContent = `${author.name} ${author.lastName} - ${post.createDate}`;
    }
  });
  buttonEdit.setAttribute("href", `/create-update.html?id=${post.id}`);
  loadingAnimation.style.display = "none";
  buttonsContainer.style.display = "block";
  postContainer.appendChild(clone);
  addEvent();
};

const displayComment = comments => {
  if (comments.length > 0) {
    for (let comment of comments) {
      users.then(resp => {
        for (let user of resp) {
          if (comment.user == user.id) {
            commentFactor = new CommentFactory(user.name, user, comment);
            commentSection.innerHTML += commentFactor.html;
          }
        }
      });
    }
  } else {
    noCommentsWarning.style.display = "flex";
    commentSection.style.display = "none";
  }
};

export { displayPost, displayComment };
