import { renderPosts, renderTags } from "./makingRequests.js";
import {
  loadingAnimation,
  searchInput,
  searchButton,
  filterTag,
} from "./utils/variables.js";
const template = document.querySelector(".templatePosts");
const recentPostsDiv = document.querySelector(".recent__posts");
const allPostsDiv = document.querySelector(".all-posts");

const tags = renderTags();

const renderTemplatePosts = post => {
  let clone = template.content.cloneNode(true);
  let img = clone.querySelector(".card__image");
  img.setAttribute("src", `${post.image}`);
  let divTags = clone.querySelector(".card-tags");
  tags.then(resp => {
    for (let tagID of post.tags) {
      const spanTag = document.createElement("span");
      spanTag.classList.add("tag");
      for (let tag of resp) {
        if (tag.id == tagID) spanTag.textContent = tag.name;
      }
      divTags.appendChild(spanTag);
    }
  });
  let tittle = clone.querySelector(".card__title-link");
  tittle.textContent = post.title;
  tittle.setAttribute("href", `/posts.html?id=${post.id}`);
  let likes = clone.querySelector(".likes");
  likes.textContent = `${post.likes}`;
  let description = clone.querySelector(".card__description");
  if (post.body.length > 85) {
    description.textContent = `${post.body.slice(0, 85)}...`;
  } else {
    description.textContent = `${post.body}`;
  }
  let card__button = clone.querySelector(".card__button");
  card__button.setAttribute("href", `/posts.html?id=${post.id}`);
  return clone;
};

const showRecentPosts = async () => {
  loadingAnimation.style.display = "block";
  let posts = await renderPosts();
  for (let post of posts.slice(0, 3)) {
    const postDone = renderTemplatePosts(post);
    recentPostsDiv.appendChild(postDone);
  }
  loadingAnimation.style.display = "none";
  recentPostsDiv.style.display = "grid";
};

const showAllPosts = async searchFilter => {
  let posts;
  if (searchFilter) {
    posts = await renderPosts(searchFilter);
  } else {
    posts = await renderPosts();
  }
  allPostsDiv.innerHTML = "";
  if (posts.length == 0) {
    allPostsDiv.innerHTML = `<p>No data found</p>`;
  } else {
    for (let post of posts) {
      const postDone = renderTemplatePosts(post);
      allPostsDiv.appendChild(postDone);
    }
  }
};

const getTags = async () => {
  const listTags = await tags;
  for (let tag of listTags) {
    filterTag.innerHTML += `<option value=${tag.id}>${tag.name}</option>`;
  }
};

const filterPostsTag = async filterValue => {
  if (filterValue == "all") return showAllPosts();
  let posts = await renderPosts();
  allPostsDiv.innerHTML = "";
  searchInput.value = "";
  for (let post of posts) {
    if (post.tags.includes(parseInt(filterValue))) {
      const postDone = renderTemplatePosts(post);
      allPostsDiv.appendChild(postDone);
    }
  }
  if (allPostsDiv.innerHTML == "")
    allPostsDiv.innerHTML = `<p>No data found</p>`;
};

searchButton.addEventListener("click", () =>
  showAllPosts(searchInput.value.trim())
);

getTags();

filterTag.addEventListener("change", () => filterPostsTag(filterTag.value));
export { showRecentPosts, showAllPosts };
