import { autoCompleteDiv, tagsDiv } from "./utils/variables.js";
import { renderTags } from "./makingRequests.js";
import { tagSearch } from "./utils/variables.js";
import { tagsSaved } from "./create-update.js";

const deleteTag = tagId => {
  for (let tag in tagsSaved) {
    if (tagsSaved[tag][0] == tagId) {
      tagsSaved.splice(tag, 1);
      reloadTags();
    }
  }
};

const showTags = (tagKey, tagName) => {
  tagsDiv.innerHTML += `
	<div class="tag__added">
		<span>${tagName}</span>
		<span class="material-icons" data-key=${tagKey}>close</span>
	</div>
	`;
  const allTags = document.querySelectorAll(".material-icons");
  allTags.forEach(singleTag => {
    singleTag.addEventListener("click", () =>
      deleteTag(singleTag.getAttribute("data-key"))
    );
  });
};

const reloadTags = () => {
  tagsDiv.innerHTML = "";
  for (let tag of tagsSaved) {
    showTags(tag[0], tag[1]);
  }
};

const verifyTag = matchTag => {
  for (let item of tagsSaved) {
    if (item[0] == matchTag.getAttribute("data-id")) {
      console.log("ya esta");
      return;
    }
  }
  tagsSaved.push([
    parseInt(matchTag.getAttribute("data-id")),
    matchTag.textContent,
  ]);
  showTags(matchTag.getAttribute("data-id"), matchTag.textContent);
};

export const searchTags = async tagValue => {
  if (tagValue.length < 1) {
    autoCompleteDiv.innerHTML = "";
    autoCompleteDiv.style.display = "none";
    return;
  }
  try {
    const tags = await renderTags(tagValue);
    if (tags.length > 0) {
      const html = tags
        .map(
          tag =>
            `<div data-id=${tag.id} class='match__tags'><h5>${tag.name}</h5></div>`
        )
        .join("");
      autoCompleteDiv.style.display = "block";
      autoCompleteDiv.innerHTML = html;
      let matchTags = document.querySelectorAll(".match__tags");
      matchTags.forEach(matchTag => {
        matchTag.addEventListener("click", () => {
          verifyTag(matchTag);
          tagSearch.value = "";
          autoCompleteDiv.innerHTML = "";
          autoCompleteDiv.style.display = "none";
        });
      });
    }
  } catch (err) {
    console.log(err);
  }
};

export { showTags };
