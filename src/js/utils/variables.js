const loadingAnimation = document.querySelector(".animation--loading");
const templatePost = document.querySelector(".post__template");
const postContainer = document.querySelector(".post__container");
const commentSection = document.querySelector(".comments");
const noCommentsWarning = document.querySelector(".no-comments__text");
const popUpDelete = document.querySelector(".modal");
const closeButton = document.querySelector(".close");
const cancelButton = document.querySelector(".cancel--button");
const searchInput = document.querySelector(".input__search");
const searchButton = document.querySelector(".button--search");
const filterTag = document.querySelector(".filter-tag");
const autoCompleteDiv = document.querySelector(".autocomplete");
const tagSearch = document.getElementById("tags");
const tagsDiv = document.querySelector(".tags__div");

export {
  tagSearch,
  tagsDiv,
  autoCompleteDiv,
  filterTag,
  searchButton,
  noCommentsWarning,
  loadingAnimation,
  templatePost,
  postContainer,
  commentSection,
  popUpDelete,
  closeButton,
  cancelButton,
  searchInput,
};
